﻿// Copyright 2014 theaigames.com (developers@theaigames.com)
// Conversion to .Net by Mark van Oudheusden (m.w.vano@gmail.com)
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at

//        http://www.apache.org/licenses/LICENSE-2.0

//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
//	
//    For the full copyright and license information, please view the LICENSE
//    file that was distributed with this source code.
using System;
using System.Collections.Generic;
using Model;

namespace Bot
{
    public class BotParser
    {
        private readonly Scanner _scan;
	    private readonly IBot _bot;
        readonly BotState _currentState;
	
	public BotParser(IBot bot)
	{
		_scan = new Scanner(Console.ReadLine());
		_bot = bot;
		_currentState = new BotState();
	}
	
	public void Run()
	{
		while(_scan.HasNext())
		{
			String line = _scan.NextLine().Trim();
			if(line.Length == 0) { continue; }
			String[] parts = line.Split(' ');
			if(parts[0].Equals("pick_starting_regions")) {
				//pick which regions you want to start with
				_currentState.SetPickableStartingRegions(parts);
				List<Region> preferredStartingRegions = _bot.GetPreferredStartingRegions(_currentState, long.Parse(parts[1]));
				String output = "";
				foreach(var region in preferredStartingRegions)
					output += region.Id + " ";
				
				Console.WriteLine(output);
			} else if(parts.Length == 3 && parts[0].Equals("go")) {
				//we need to do a move
				String output = "";
				if(parts[1].Equals("place_armies")) 
				{
					//place armies
					List<PlaceArmiesMove> placeArmiesMoves = _bot.GetPlaceArmiesMoves(_currentState, long.Parse(parts[2]));
					foreach(var move in placeArmiesMoves)
						output += move.GetString() + ",";
				} 
				else if(parts[1].Equals("attack/transfer")) 
				{
					//attack/transfer
					List<AttackTransferMove> attackTransferMoves = _bot.GetAttackTransferMoves(_currentState, long.Parse(parts[2]));
					foreach(var move in attackTransferMoves)
						output += move.GetString() + ",";
				}
				if(output.Length > 0)
					Console.WriteLine(output);
				else
					Console.WriteLine("No moves");
			} else if(parts.Length == 3 && parts[0].Equals("settings")) {
				//update settings
				_currentState.UpdateSettings(parts[1], parts[2]);
			} else if(parts[0].Equals("setup_map")) {
				//initial full map is given
				_currentState.SetupMap(parts);
			} else if(parts[0].Equals("update_map")) {
				//all visible regions are given
				_currentState.UpdateMap(parts);
			} else {
				Console.Error.WriteLine("Unable to parse line \"{0}\"\n", line);
			}
		}
	}
    }
}
