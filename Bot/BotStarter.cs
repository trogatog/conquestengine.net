﻿// Copyright 2014 theaigames.com (developers@theaigames.com)
// Conversion to .Net by Mark van Oudheusden (m.w.vano@gmail.com)
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at

//        http://www.apache.org/licenses/LICENSE-2.0

//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
//	
//    For the full copyright and license information, please view the LICENSE
//    file that was distributed with this source code.
using System;
using System.Collections.Generic;
using System.Linq;
using Model;

namespace Bot
{
    public class BotStarter : IBot
    {
        public List<Region> GetPreferredStartingRegions(BotState state, long timeOut)
	{
		const int m = 6;
        var preferredStartingRegions = new List<Region>();
		for(int i=0; i<m; i++)
		{
		    var random = new Random();
			double rand = random.NextDouble();
			var r = (int) (rand*state.GetPickableStartingRegions().Count);
			int regionId = state.GetPickableStartingRegions()[r-1].Id;
			Region region = state.GetFullMap().GetRegionById(regionId);

			if(!preferredStartingRegions.Contains(region))
				preferredStartingRegions.Add(region);
			else
				i--;
		}
		
		return preferredStartingRegions;
	}

	/**
	 * This method is called for at first part of each round. This example puts two armies on random regions
	 * until he has no more armies left to place.
	 * @return The list of PlaceArmiesMoves for one round
	 */
        public List<PlaceArmiesMove> GetPlaceArmiesMoves(BotState state, long timeOut) 
	{

        var placeArmiesMoves = new List<PlaceArmiesMove>();
		String myName = state.GetMyPlayerName();
		const int armies = 2;
		int armiesLeft = state.GetStartingArmies();
		List<Region> visibleRegions = state.GetVisibleMap().GetRegions();
		
		while(armiesLeft > 0)
		{
		    var random = new Random();
			double rand = random.NextDouble();
			var r = (int) (rand*visibleRegions.Count);
		    Region region = visibleRegions[r - 1];
			
			if(region.OwnedByPlayer(myName))
			{
				placeArmiesMoves.Add(new PlaceArmiesMove(myName, region, armies));
				armiesLeft -= armies;
			}
		}
		
		return placeArmiesMoves;
	}

	/**
	 * This method is called for at the second part of each round. This example attacks if a region has
	 * more than 6 armies on it, and transfers if it has less than 6 and a neighboring owned region.
	 * @return The list of PlaceArmiesMoves for one round
	 */
	public List<AttackTransferMove> GetAttackTransferMoves(BotState state, long timeOut) 
	{
		var attackTransferMoves = new List<AttackTransferMove>();
		String myName = state.GetMyPlayerName();
		const int armies = 5;
		
		foreach (var fromRegion in state.GetVisibleMap().GetRegions())
		{
			if(fromRegion.OwnedByPlayer(myName)) //do an attack
			{
                var possibleToRegions = new List<Region>();
				possibleToRegions.AddRange(fromRegion.Neighbors);
				
				while(possibleToRegions.Any())
				{
				    var random = new Random();
					double rand = random.NextDouble();
					var r = (int) (rand*possibleToRegions.Count);
					Region toRegion = possibleToRegions[r];
					
					if(!toRegion.PlayerName.Equals(myName) && fromRegion.Armies > 6) //do an attack
					{
						attackTransferMoves.Add(new AttackTransferMove(myName, fromRegion, toRegion, armies));
						break;
					}
				
                    if(toRegion.PlayerName.Equals(myName) && fromRegion.Armies > 1) //do a transfer
				    {
				        attackTransferMoves.Add(new AttackTransferMove(myName, fromRegion, toRegion, armies));
				        break;
				    }
				    
                    possibleToRegions.Remove(toRegion);
				}
			}
		}
		
		return attackTransferMoves;
	}
    }
}
