﻿// Copyright 2014 theaigames.com (developers@theaigames.com)
// Conversion to .Net by Mark van Oudheusden (m.w.vano@gmail.com)
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at

//        http://www.apache.org/licenses/LICENSE-2.0

//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
//	
//    For the full copyright and license information, please view the LICENSE
//    file that was distributed with this source code.
using System;
using System.Collections.Generic;
using System.Linq;
using Model;

namespace Bot
{
    public class BotState
    {
        private String _myName = string.Empty;
        private String _opponentName = string.Empty;

        private readonly Map _fullMap = new Map();
        //This map is known from the start, contains all the regions and how they are connected, doesn't change after initialization
        private Map _visibleMap; //This map represents everything the player can see, updated at the end of each round.

        private readonly List<Region> _pickableStartingRegions;
            //2 randomly chosen regions from each superregion are given, which the bot can chose to start with

        private int _startingArmies; //number of armies the player can place on map

        private int _roundNumber;

        public BotState()
        {
            _pickableStartingRegions = new List<Region>();
            _roundNumber = 0;
        }

        public void UpdateSettings(String key, String value)
        {
            if (key.Equals("your_bot")) //bot's own name
                _myName = value;
            else if (key.Equals("opponent_bot")) //opponent's name
                _opponentName = value;
            else if (key.Equals("starting_armies"))
            {
                _startingArmies = int.Parse(value);
                _roundNumber++; //next round
            }
        }

        //initial map is given to the bot with all the information except for player and armies info
        public void SetupMap(String[] mapInput)
        {
            int i;
            int superRegionId;

            if (mapInput[1].Equals("super_regions"))
            {
                for (i = 2; i < mapInput.Length; i++)
                {
                    try
                    {
                        superRegionId = int.Parse(mapInput[i]);
                        i++;
                        int reward = int.Parse(mapInput[i]);
                        _fullMap.Add(new SuperRegion(superRegionId, reward));
                    }
                    catch (Exception)
                    {
                        Console.Error.WriteLine("Unable to parse SuperRegions");
                    }
                }
            }
            else if (mapInput[1].Equals("regions"))
            {
                for (i = 2; i < mapInput.Length; i++)
                {
                    try
                    {
                        int regionId = int.Parse(mapInput[i]);
                        i++;
                        superRegionId = int.Parse(mapInput[i]);
                        SuperRegion superRegion = _fullMap.GetSuperRegionById(superRegionId);
                        _fullMap.Add(new Region(regionId, superRegion));
                    }
                    catch (Exception e)
                    {
                        Console.Error.WriteLine("Unable to parse Regions " + e.Message);
                    }
                }
            }
            else if (mapInput[1].Equals("neighbors"))
            {
                for (i = 2; i < mapInput.Length; i++)
                {
                    try
                    {
                        var region = _fullMap.GetRegionById(int.Parse(mapInput[i]));
                        i++;
                        var neighborIds = mapInput[i].Split(',');
                        for (int j = 0; j < neighborIds.Length; j++)
                        {
                            var neighbor = _fullMap.GetRegionById(int.Parse(neighborIds[j]));
                            region.AddNeighbor(neighbor);
                        }
                    }
                    catch (Exception e)
                    {
                        Console.Error.WriteLine("Unable to parse Neighbors " + e.Message);
                    }
                }
            }
        }

        //regions from wich a player is able to pick his preferred starting regions
        public void SetPickableStartingRegions(String[] mapInput)
        {
            for (int i = 2; i < mapInput.Length; i++)
            {
                try
                {
                    int regionId = int.Parse(mapInput[i]);
                    Region pickableRegion = _fullMap.GetRegionById(regionId);
                    _pickableStartingRegions.Add(pickableRegion);
                }
                catch (Exception e)
                {
                    Console.Error.WriteLine("Unable to parse pickable regions " + e.Message);
                }
            }
        }

        //visible regions are given to the bot with player and armies info
        public void UpdateMap(String[] mapInput)
        {
            _visibleMap = _fullMap.GetMapCopy();
            for (var i = 1; i < mapInput.Length; i++)
            {
                try
                {
                    var region = _visibleMap.GetRegionById(int.Parse(mapInput[i]));
                    var playerName = mapInput[i + 1];
                    var armies = int.Parse(mapInput[i + 2]);

                    region.PlayerName = (playerName);
                    region.Armies = (armies);
                    i += 2;
                }
                catch (Exception e)
                {
                    Console.Error.WriteLine("Unable to parse Map Update " + e.Message);
                }
            }
            var unknownRegions = _visibleMap.Regions.Where(region => region.PlayerName.Equals("unknown")).ToList();

            //remove regions which are unknown.

            foreach (var unknownRegion in unknownRegions)
                _visibleMap.GetRegions().Remove(unknownRegion);
        }

        public String GetMyPlayerName()
        {
            return _myName;
        }

        public String GetOpponentPlayerName()
        {
            return _opponentName;
        }

        public int GetStartingArmies()
        {
            return _startingArmies;
        }

        public int GetRoundNumber()
        {
            return _roundNumber;
        }

        public Map GetVisibleMap()
        {
            return _visibleMap;
        }

        public Map GetFullMap()
        {
            return _fullMap;
        }

        public List<Region> GetPickableStartingRegions()
        {
            return _pickableStartingRegions;
        }
    }
}