﻿// Copyright 2014 theaigames.com (developers@theaigames.com)
// Conversion to .Net by Mark van Oudheusden (m.w.vano@gmail.com)
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at

//        http://www.apache.org/licenses/LICENSE-2.0

//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
//	
//    For the full copyright and license information, please view the LICENSE
//    file that was distributed with this source code.
using System;
using System.Collections.Generic;
using System.Linq;
using Model;

namespace Main
{
    public class Parser
    {
        private readonly Map _map;

        public Parser(Map map)
        {
            _map = map;
        }

        public List<Move> ParseMoves(String input, Player player)
        {
            var moves = new List<Move>();

            try
            {
                input = input.Trim();
                if (input.Length <= 1)
                    return moves;

                String[] split = input.Split(',');

                for (int i = 0; i < split.Length; i++)
                {
                    if (i > 50)
                    {
                        player.Bot.AddToDump("Maximum number of moves reached, max 50 moves are allowed");
                        break;
                    }
                    Move move = ParseMove(split[i], player);
                    if (move != null)
                        moves.Add(move);
                }
            }
            catch (Exception)
            {
                player.Bot.AddToDump("Move input is null");
            }
            return moves;
        }

        //returns the correct Move. Null if input is incorrect.
        private Move ParseMove(String input, Player player)
        {
            int armies = -1;

            var split = input.Trim().Split(' ').ToList();
            if (split.Count == 1 && split[0] == string.Empty)
                return null;
            if (!split[0].Equals(player.Name))
            {
                errorOut("Incorrect player name or move format incorrect", input, player);
                return null;
            }

            if (split[1].Equals("place_armies"))
            {
                Region region = ParseRegion(split[2], input, player);

                try { armies = int.Parse(split[3]); }
                catch (Exception) { errorOut("Number of armies input incorrect", input, player); }

                if (!(region == null || armies == -1))
                    return new PlaceArmiesMove(player.Name, region, armies);
                return null;
            }
            
            if (split[1].Equals("attack/transfer"))
            {
                Region fromRegion = ParseRegion(split[2], input, player);
                Region toRegion = ParseRegion(split[3], input, player);

                try { armies = int.Parse(split[4]); }
                catch (Exception) { errorOut("Number of armies input incorrect", input, player); }

                if (!(fromRegion == null || toRegion == null || armies == -1))
                    return new AttackTransferMove(player.Name, fromRegion, toRegion, armies);
                return null;
            }

            errorOut("Bot's move format incorrect", input, player);
            return null;
        }

        //parse the region given the id string.
        private Region ParseRegion(String regionId, String input, Player player)
        {
            int id;

            try { id = int.Parse(regionId); }
            catch (Exception) { errorOut("Region id input incorrect", input, player); return null; }

            Region region = _map.GetRegionById(id);

            return region;
        }

        public List<Region> ParsePreferredStartingRegions(String input, List<Region> pickableRegions, Player player)
        {
            var preferredStartingRegions = new List<Region>();

            try
            {
                const int nrOfPreferredStartingRegions = 6;
                String[] split = input.Split(' ');

                for (int i = 0; i < nrOfPreferredStartingRegions; i++)
                {
                    try
                    {
                        Region r = ParseRegion(split[i], input, player);

                        if (pickableRegions.Contains(r))
                        {
                            if (!preferredStartingRegions.Contains(r))
                                preferredStartingRegions.Add(r);
                            else
                            {
                                errorOut("preferred starting regions: Same region appears more than once", input, player);
                                return null;
                            }
                        }
                        else
                        {
                            errorOut("preferred starting regions: Chosen region is not in the given pickable regions list", input, player);
                            return null;
                        }
                    }
                    catch (Exception)
                    { //player has not returned enough preferred regions
                        errorOut("preferred starting regions: Player did not return enough preferred starting regions", input, player);
                        return null;
                    }
                }
                return preferredStartingRegions;
            }
            catch (Exception)
            {
                player.Bot.AddToDump("Preferred starting regions input is null");
                return null;
            }
        }

        private void errorOut(String error, String input, Player player)
        {
            player.Bot.AddToDump("Parse error: " + error + " (" + input + ")");
        }

    }
}
