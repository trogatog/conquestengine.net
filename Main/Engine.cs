﻿// Copyright 2014 theaigames.com (developers@theaigames.com)
// Conversion to .Net by Mark van Oudheusden (m.w.vano@gmail.com)
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at

//        http://www.apache.org/licenses/LICENSE-2.0

//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
//	
//    For the full copyright and license information, please view the LICENSE
//    file that was distributed with this source code.

using System;
using System.Collections.Generic;
using System.Linq;
using Main.Extensions;
using Model;

namespace Main
{
    internal class Engine
    {

        private readonly Player _player1;
        private readonly Player _player2;
        private readonly Map _map;
        private readonly Parser _parser;
        private int _roundNumber;
        private readonly List<MoveResult> _fullPlayedGame;
        private readonly List<MoveResult> _player1PlayedGame;
        private readonly List<MoveResult> _player2PlayedGame;
        private readonly List<Move> _opponentMovesPlayer1;
        private readonly List<Move> _opponentMovesPlayer2;
        private readonly MoveQueue _moveQueue;

        public Engine(Map initMap, Player player1, Player player2)
        {
            _map = initMap;
            _player1 = player1;
            _player2 = player2;
            _roundNumber = 1;
            _moveQueue = new MoveQueue(player1, player2);

            _parser = new Parser(_map);

            _fullPlayedGame = new List<MoveResult>();
            _player1PlayedGame = new List<MoveResult>();
            _player2PlayedGame = new List<MoveResult>();
            _opponentMovesPlayer1 = new List<Move>();
            _opponentMovesPlayer2 = new List<Move>();
        }

        public void PlayRound()
        {
            GetMoves(_player1.Bot.GetPlaceArmiesMoves(2000), _player1);
            GetMoves(_player2.Bot.GetPlaceArmiesMoves(2000), _player2);

            ExecutePlaceArmies();

            GetMoves(_player1.Bot.GetAttackTransferMoves(2000), _player1);
            GetMoves(_player2.Bot.GetAttackTransferMoves(2000), _player2);

            ExecuteAttackTransfer();

            _moveQueue.Clear();
            RecalculateStartingArmies();
            SendAllInfo();
            _fullPlayedGame.Add(null); //indicates round end	
            _player1PlayedGame.Add(null);
            _player2PlayedGame.Add(null);
            _roundNumber++;
        }

        public void DistributeStartingRegions()
        {
            var pickableRegions = new List<Region>();
            const int nrOfStartingRegions = 3;
            int regionsAdded = 0;

            //pick semi random regions to start with
            foreach (var superRegion in _map.SuperRegions)
            {
                int nrOfRegions = superRegion.SubRegions.Count;
                while (regionsAdded < 2)
                {
                    var random = new Random();
                    double rand = random.NextDouble();
                    var randomRegionId = (int)(rand * nrOfRegions);
                    Region randomRegion = superRegion.SubRegions[randomRegionId]; //get one random subregion from superRegion
                    if (!pickableRegions.Contains(randomRegion))
                    {
                        pickableRegions.Add(randomRegion);
                        regionsAdded++;
                    }
                }
                regionsAdded = 0;
            }

            //get the preferred starting regions from the players
            var p1Regions = _parser.ParsePreferredStartingRegions(_player1.Bot.GetPreferredStartingArmies(10000, pickableRegions), pickableRegions, _player1);
            var p2Regions = _parser.ParsePreferredStartingRegions(_player2.Bot.GetPreferredStartingArmies(10000, pickableRegions), pickableRegions, _player2);
            var givenP1Regions = new List<Region>();
            var givenP2Regions = new List<Region>();

            //if the bot did not correctly return his starting regions, get some random ones
            if (p1Regions == null)
            {
                p1Regions = new List<Region>();
            }
            if (p2Regions == null)
            {
                p2Regions = new List<Region>();
            }

            p1Regions.AddRange(GetRandomStartingRegions(pickableRegions));
            p2Regions.AddRange(GetRandomStartingRegions(pickableRegions));

            //distribute the starting regions
            int i1 = 0;
            int i2 = 0;
            int n = 0;

            while (n < nrOfStartingRegions)
            {
                Region p1Region = p1Regions[i1];
                Region p2Region = p2Regions[i2];

                if (givenP2Regions.Contains(p1Region))
                {
                    //preferred region for player1 is not given to player2 already
                    i1++;
                }
                else if (givenP1Regions.Contains(p2Region))
                {
                    //preferred region for player2 is not given to player1 already
                    i2++;
                }
                else if (p1Region != p2Region)
                {
                    p1Region.PlayerName = (_player1.Name);
                    p2Region.PlayerName = (_player2.Name);
                    givenP1Regions.Add(p1Region);
                    givenP2Regions.Add(p2Region);
                    n++;
                    i1++;
                    i2++;
                }
                else
                {
                    //random player gets the region if same preference
                    var random = new Random();
                    double rand = random.NextDouble();
                    if (rand < 0.5)
                    {
                        i1++;
                    }
                    else
                    {
                        i2++;
                    }
                }
            }

            _fullPlayedGame.Add(new MoveResult(null, _map.GetMapCopy()));
            _player1PlayedGame.Add(new MoveResult(null, _map.GetVisibleMapCopyForPlayer(_player1)));
            _player2PlayedGame.Add(new MoveResult(null, _map.GetVisibleMapCopyForPlayer(_player2)));
        }

        private IEnumerable<Region> GetRandomStartingRegions(IEnumerable<Region> pickableRegions)
        {
            var startingRegions = new List<Region>(pickableRegions);
            startingRegions.Shuffle();

            startingRegions = startingRegions.GetRange(0, 6);
            return startingRegions;
        }

        private void GetMoves(String movesInput, Player player)
        {
            List<Move> moves = _parser.ParseMoves(movesInput, player);

            foreach (var move in moves)
            {
                try //PlaceArmiesMove
                {
                    var plm = (PlaceArmiesMove)move;
                    QueuePlaceArmies(plm);
                }
                catch (Exception) //AttackTransferMove
                {
                    var atm = (AttackTransferMove)move;
                    QueueAttackTransfer(atm);
                }
            }
        }

        private void QueuePlaceArmies(PlaceArmiesMove plm)
        {
            //should not ever happen
            if (plm == null)
            {
                Console.Error.WriteLine("Error on place_armies input.");
                return;
            }

            Region region = plm.Region;
            Player player = GetPlayer(plm.GetPlayerName());
            int armies = plm.Armies;

            //check legality
            if (region.OwnedByPlayer(player.Name))
            {
                if (armies < 1)
                {
                    plm.SetIllegalMove(" place-armies " + "cannot place less than 1 army");
                }
                else
                {
                    if (armies > player.ArmiesLeft) //player wants to place more armies than he has left
                        plm.Armies = (player.ArmiesLeft); //place all armies he has left
                    if (player.ArmiesLeft <= 0)
                        plm.SetIllegalMove(" place-armies " + "no armies left to place");

                    player.ArmiesLeft = (player.ArmiesLeft - plm.Armies);
                }
            }
            else
                plm.SetIllegalMove(plm.Region.Id + " place-armies " + " not owned");

            _moveQueue.AddMove(plm);
        }

        private void QueueAttackTransfer(AttackTransferMove atm)
        {
            //should not ever happen
            if (atm == null)
            {
                Console.Error.WriteLine("Error on attack/transfer input.");
                return;
            }

            var fromRegion = atm.FromRegion;
            var toRegion = atm.ToRegion;
            var player = GetPlayer(atm.GetPlayerName());
            var armies = atm.Armies;

            //check legality
            if (fromRegion.OwnedByPlayer(player.Name))
            {
                if (fromRegion.IsNeighbor(toRegion))
                {
                    if (armies < 1)
                        atm.SetIllegalMove(" attack/transfer " + "cannot use less than 1 army");
                }
                else
                    atm.SetIllegalMove(atm.ToRegion.Id + " attack/transfer " + "not a neighbor");
            }
            else
                atm.SetIllegalMove(atm.FromRegion.Id + " attack/transfer " + "not owned");

            _moveQueue.AddMove(atm);
        }

        //Moves have already been checked if they are legal
        private void ExecutePlaceArmies()
        {
            foreach (var move in _moveQueue.PlaceArmiesMoves)
            {
                if (move.GetIllegalMove().Equals("")) //the move is not illegal
                    move.Region.Armies = (move.Region.Armies + move.Armies);

                Map mapCopy = _map.GetMapCopy();
                _fullPlayedGame.Add(new MoveResult(move, mapCopy));
                if (_map.VisibleRegionsForPlayer(_player1).Contains(move.Region))
                {
                    _player1PlayedGame.Add(new MoveResult(move, _map.GetVisibleMapCopyForPlayer(_player1))); //for the game file
                    if (move.GetPlayerName().Equals(_player2.Name))
                        _opponentMovesPlayer1.Add(move); //for the opponent_moves output
                }
                if (_map.VisibleRegionsForPlayer(_player2).Contains(move.Region))
                {
                    _player2PlayedGame.Add(new MoveResult(move, _map.GetVisibleMapCopyForPlayer(_player2))); //for the game file
                    if (move.GetPlayerName().Equals(_player1.Name))
                        _opponentMovesPlayer2.Add(move); //for the opponent_moves output
                }
            }
        }

        private void ExecuteAttackTransfer()
        {
            List<Region> visibleRegionsPlayer1Map = _map.VisibleRegionsForPlayer(_player1);
            List<Region> visibleRegionsPlayer2Map = _map.VisibleRegionsForPlayer(_player2);
            List<Region> visibleRegionsPlayer1OldMap = visibleRegionsPlayer1Map;
            List<Region> visibleRegionsPlayer2OldMap = visibleRegionsPlayer2Map;
            var usedRegions = new List<List<int>>();
            for (int i = 0; i <= 42; i++)
            {
                usedRegions.Add(new List<int>());
            }
            Map oldMap = _map.GetMapCopy();

            int moveNr = 1;
            bool previousMoveWasIllegal = false;
            String previousMovePlayer = "";
            while (_moveQueue.HasNextAttackTransferMove())
            {
                AttackTransferMove move = _moveQueue.GetNextAttackTransferMove(moveNr, previousMovePlayer, previousMoveWasIllegal);

                if (move.GetIllegalMove().Equals("")) //the move is not illegal
                {
                    Region fromRegion = move.FromRegion;
                    Region oldFromRegion = oldMap.GetRegionById(move.FromRegion.Id);
                    Region toRegion = move.ToRegion;
                    Player player = GetPlayer(move.GetPlayerName());

                    if (fromRegion.OwnedByPlayer(player.Name)) //check if the fromRegion still belongs to this player
                    {
                        if (!usedRegions[fromRegion.Id].Contains(toRegion.Id)) //between two regions there can only be attacked/transfered once
                        {
                            if (oldFromRegion.Armies > 1) //there are still armies that can be used
                            {
                                if (oldFromRegion.Armies < fromRegion.Armies && oldFromRegion.Armies - 1 < move.Armies) //not enough armies on fromRegion at the start of the round?
                                    move.Armies = (oldFromRegion.Armies - 1); //move the maximal number.
                                else if (oldFromRegion.Armies >= fromRegion.Armies && fromRegion.Armies - 1 < move.Armies) //not enough armies on fromRegion currently?
                                    move.Armies = (fromRegion.Armies - 1); //move the maximal number.

                                oldFromRegion.Armies = (oldFromRegion.Armies - move.Armies); //update oldFromRegion so new armies cannot be used yet

                                if (toRegion.OwnedByPlayer(player.Name)) //transfer
                                {
                                    if (fromRegion.Armies > 1)
                                    {
                                        fromRegion.Armies = (fromRegion.Armies - move.Armies);
                                        toRegion.Armies = (toRegion.Armies + move.Armies);
                                        usedRegions[fromRegion.Id].Add(toRegion.Id);
                                    }
                                    else
                                        move.SetIllegalMove(move.FromRegion.Id + " transfer " + "only has 1 army");
                                }
                                else //attack
                                {
                                    doAttack(move);
                                    usedRegions[fromRegion.Id].Add(toRegion.Id);
                                }
                            }
                            else
                                move.SetIllegalMove(move.FromRegion.Id + " attack/transfer " + "has used all available armies");
                        }
                        else
                            move.SetIllegalMove(move.FromRegion.Id + " attack/transfer " + "has already attacked/transfered to this region");
                    }
                    else
                        move.SetIllegalMove(move.FromRegion.Id + " attack/transfer " + "was taken this round");
                }

                visibleRegionsPlayer1Map = _map.VisibleRegionsForPlayer(_player1);
                visibleRegionsPlayer2Map = _map.VisibleRegionsForPlayer(_player2);

                _fullPlayedGame.Add(new MoveResult(move, _map.GetMapCopy()));
                if (visibleRegionsPlayer1Map.Contains(move.FromRegion) || visibleRegionsPlayer1Map.Contains(move.ToRegion) ||
                    visibleRegionsPlayer1OldMap.Contains(move.ToRegion))
                {
                    _player1PlayedGame.Add(new MoveResult(move, _map.GetVisibleMapCopyForPlayer(_player1))); //for the game file
                    if (move.GetPlayerName().Equals(_player2.Name))
                        _opponentMovesPlayer1.Add(move); //for the opponent_moves output
                }
                if (visibleRegionsPlayer2Map.Contains(move.FromRegion) || visibleRegionsPlayer2Map.Contains(move.ToRegion) ||
                    visibleRegionsPlayer2OldMap.Contains(move.ToRegion))
                {
                    _player2PlayedGame.Add(new MoveResult(move, _map.GetVisibleMapCopyForPlayer(_player2))); //for the game file
                    if (move.GetPlayerName().Equals(_player1.Name))
                        _opponentMovesPlayer2.Add(move); //for the opponent_moves output
                }

                visibleRegionsPlayer1OldMap = visibleRegionsPlayer1Map;
                visibleRegionsPlayer2OldMap = visibleRegionsPlayer2Map;

                //set some stuff to know what next move to get
                if (move.GetIllegalMove().Equals(""))
                {
                    previousMoveWasIllegal = false;
                    moveNr++;
                }
                else
                {
                    previousMoveWasIllegal = true;
                }
                previousMovePlayer = move.GetPlayerName();

            }
        }

        //see wiki.warlight.net/index.php/Combat_Basics
        private void doAttack(AttackTransferMove move)
        {
            Region fromRegion = move.FromRegion;
            Region toRegion = move.ToRegion;
            int defendingArmies = toRegion.Armies;

            int defendersDestroyed = 0;
            int attackersDestroyed = 0;

            if (fromRegion.Armies > 1)
            {
                int attackingArmies;
                if (fromRegion.Armies - 1 >= move.Armies) //are there enough armies on fromRegion?
                    attackingArmies = move.Armies;
                else
                    attackingArmies = fromRegion.Armies - 1;

                var random = new Random();
                for (int t = 1; t <= attackingArmies; t++) //calculate how much defending armies are destroyed
                {
                    double rand = random.NextDouble();
                    if (rand < 0.6) //60% chance to destroy one defending army
                        defendersDestroyed++;
                }
                for (int t = 1; t <= defendingArmies; t++) //calculate how much attacking armies are destroyed
                {
                    double rand = random.NextDouble();
                    if (rand < 0.7) //70% chance to destroy one attacking army
                        attackersDestroyed++;
                }

                if (attackersDestroyed >= attackingArmies)
                {
                    if (defendersDestroyed >= defendingArmies)
                        defendersDestroyed = defendingArmies - 1;

                    attackersDestroyed = attackingArmies;
                }

                //process result of attack
                if (defendersDestroyed >= defendingArmies) //attack success
                {
                    fromRegion.Armies = (fromRegion.Armies - attackingArmies);
                    toRegion.PlayerName = (move.GetPlayerName());
                    toRegion.Armies = (attackingArmies - attackersDestroyed);
                }
                else //attack fail
                {
                    fromRegion.Armies = (fromRegion.Armies - attackersDestroyed);
                    toRegion.Armies = (toRegion.Armies - defendersDestroyed);
                }

            }
            else
                move.SetIllegalMove(move.FromRegion.Id + " attack " + "only has 1 army");
        }

        public Player WinningPlayer()
        {
            if (!_map.OwnedRegionsByPlayer(_player1).Any())
                return _player2;

            if (!_map.OwnedRegionsByPlayer(_player2).Any())
                return _player1;

            return null;
        }

        //calculate how many armies each player is able to place on the map for the next round
        public void RecalculateStartingArmies()
        {
            _player1.ArmiesLeft = (_player1.ArmiesPerTurn);
            _player2.ArmiesLeft = (_player2.ArmiesPerTurn);

            foreach (var superRegion in _map.SuperRegions)
            {
                Player player = GetPlayer(superRegion.OwnedByPlayer());
                if (player != null)
                    player.ArmiesLeft = (player.ArmiesLeft + superRegion.ArmiesReward);
            }
        }

        public void SendAllInfo()
        {
            sendStartingArmiesInfo(_player1);
            sendStartingArmiesInfo(_player2);
            SendUpdateMapInfo(_player1);
            SendUpdateMapInfo(_player2);
            SendOpponentMovesInfo(_player1);
            _opponentMovesPlayer1.Clear();
            SendOpponentMovesInfo(_player2);
            _opponentMovesPlayer2.Clear();
        }

        //inform the player about how much armies he can place at the start next round
        private void sendStartingArmiesInfo(Player player)
        {
            String updateStartingArmiesString = "settings starting_armies";

            updateStartingArmiesString += " " + player.ArmiesLeft;

            //Console.WriteLine("sending to " + player.Name + ": " + updateStartingArmiesString);
            player.Bot.WriteInfo(updateStartingArmiesString);
        }

        //inform the player about how his visible map looks now
        private void SendUpdateMapInfo(Player player)
        {
            var visibleRegions = _map.VisibleRegionsForPlayer(player);
            var updateMapString = "update_map";
            foreach (var region in visibleRegions)
            {
                var id = region.Id;
                var playerName = region.PlayerName;
                var armies = region.Armies;

                updateMapString += " " + id + " " + playerName + " " + armies;
            }
            player.Bot.WriteInfo(updateMapString);
        }

        private void SendOpponentMovesInfo(Player player)
        {
            var opponentMovesString = "opponent_moves ";
            var opponentMoves = new List<Move>();

            if (player == _player1)
                opponentMoves = _opponentMovesPlayer1;
            else if (player == _player2)
                opponentMoves = _opponentMovesPlayer2;

            foreach (var move in opponentMoves)
            {
                if (move.GetIllegalMove().Equals(""))
                {
                    try
                    {
                        var plm = (PlaceArmiesMove)move;
                        opponentMovesString += plm.GetString() + " ";
                    }
                    catch (Exception)
                    {
                        var atm = (AttackTransferMove)move;
                        opponentMovesString += atm.GetString() + " ";
                    }
                }
            }

            opponentMovesString = opponentMovesString.Substring(0, opponentMovesString.Length - 1);

            player.Bot.WriteInfo(opponentMovesString);
        }

        private Player GetPlayer(String playerName)
        {
            if (_player1.Name.Equals(playerName))
                return _player1;
            if (_player2.Name.Equals(playerName))
                return _player2;
            return null;
        }

        public List<MoveResult> FullPlayedGame{get { return _fullPlayedGame; }} 

        public List<MoveResult> Player1PlayedGame{get { return _player1PlayedGame; }}
        public List<MoveResult> Player2PlayedGame { get { return _player2PlayedGame; } } 
        public int RoundNumber{get { return _roundNumber; }}
    }

}