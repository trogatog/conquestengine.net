﻿// Copyright 2014 theaigames.com (developers@theaigames.com)
// Conversion to .Net by Mark van Oudheusden (m.w.vano@gmail.com)
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at

//        http://www.apache.org/licenses/LICENSE-2.0

//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
//	
//    For the full copyright and license information, please view the LICENSE
//    file that was distributed with this source code.
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Threading;
using IO;
using Main.Extensions;
using Model;

namespace Main
{
    public class RunGame
    {
        private readonly string _playerName1;
        private readonly string _playerName2;
        private int _player1Wins = 0;
        private int _player2Wins = 0;

        private readonly string _bot1Dir;
        private readonly string _bot2Dir;
        private readonly int _numGamesToPlay;

        private Engine _engine;

        //private DB db;

        public RunGame(IList<string> args)
        {
            _bot1Dir = args[0];
            _bot2Dir = args[1];
            if (args.Count >= 3)
            {
                if (!int.TryParse(args[2], out _numGamesToPlay))
                {
                    _numGamesToPlay = 1;
                    Console.Error.Write("Did not provide a number of games to play or else the format was incorrect (int). Setting valid games to 1\r\n");
                }
            }
            else
            {
                _numGamesToPlay = 1;
                Console.Error.Write("Did not provide a number of games to play or else the format was incorrect (int). Setting valid games to 1\r\n");
            }
            _playerName1 = "player1";
            _playerName2 = "player2";
        }

        public void Go()
        {
            var rounds = _numGamesToPlay;
            var gameNumber = 1;
            while (rounds-- > 0)
            {
                //setup the bots
                var bot1 = new IORobot(_bot1Dir);
                var bot2 = new IORobot(_bot2Dir);

                const int startingArmies = 5;
                var player1 = new Player(_playerName1, bot1, startingArmies);
                var player2 = new Player(_playerName2, bot2, startingArmies);

                //setup the map
                Map initMap = makeInitMap();
                Map map = setupMap(initMap);

                //start the engine
                _engine = new Engine(map, player1, player2);

                //send the bots the info they need to start
                bot1.WriteInfo("settings your_bot " + player1.Name);
                bot1.WriteInfo("settings opponent_bot " + player2.Name);
                bot2.WriteInfo("settings your_bot " + player2.Name);
                bot2.WriteInfo("settings opponent_bot " + player1.Name);
                SendSetupMapInfo(player1.Bot, initMap);
                SendSetupMapInfo(player2.Bot, initMap);
                _engine.DistributeStartingRegions(); //decide the player's starting regions
                _engine.RecalculateStartingArmies();
                //calculate how much armies the players get at the start of the round (depending on owned SuperRegions)
                _engine.SendAllInfo();

                //play the game
                while (_engine.WinningPlayer() == null && _engine.RoundNumber <= 100)
                {
                    bot1.AddToDump("Round " + _engine.RoundNumber + "\n");
                    bot2.AddToDump("Round " + _engine.RoundNumber + "\n");
                    _engine.PlayRound();
                }

                Finish(bot1, bot2, gameNumber);

                if (_engine.WinningPlayer() == null)
                {
                    rounds++;
                }
                else
                {
                    gameNumber++;
                }
            }
            File.WriteAllText(string.Format("{0}_Player1Wins_{1}_times_Player2Wins_{2}_times", ConfigurationManager.AppSettings["outputDirectory"], _player1Wins, _player2Wins), string.Empty);
        }

        //aanpassen en een QPlayer class maken? met eigen finish
        private void Finish(IORobot bot1, IORobot bot2, int gameNumber)
        {
            bot1.Finish();
            Thread.Sleep(200);

            bot2.Finish();
            Thread.Sleep(200);

            Thread.Sleep(200);

            if (_engine.WinningPlayer() != null)
                SaveGame(bot1, bot2, gameNumber);
        }

        //tijdelijk handmatig invoeren
        private Map makeInitMap()
        {
            var map = new Map();
            var northAmerica = new SuperRegion(1, 5);
            var southAmerica = new SuperRegion(2, 2);
            var europe = new SuperRegion(3, 5);
            var afrika = new SuperRegion(4, 3);
            var azia = new SuperRegion(5, 7);
            var australia = new SuperRegion(6, 2);

            var region1 = new Region(1, northAmerica);
            var region2 = new Region(2, northAmerica);
            var region3 = new Region(3, northAmerica);
            var region4 = new Region(4, northAmerica);
            var region5 = new Region(5, northAmerica);
            var region6 = new Region(6, northAmerica);
            var region7 = new Region(7, northAmerica);
            var region8 = new Region(8, northAmerica);
            var region9 = new Region(9, northAmerica);
            var region10 = new Region(10, southAmerica);
            var region11 = new Region(11, southAmerica);
            var region12 = new Region(12, southAmerica);
            var region13 = new Region(13, southAmerica);
            var region14 = new Region(14, europe);
            var region15 = new Region(15, europe);
            var region16 = new Region(16, europe);
            var region17 = new Region(17, europe);
            var region18 = new Region(18, europe);
            var region19 = new Region(19, europe);
            var region20 = new Region(20, europe);
            var region21 = new Region(21, afrika);
            var region22 = new Region(22, afrika);
            var region23 = new Region(23, afrika);
            var region24 = new Region(24, afrika);
            var region25 = new Region(25, afrika);
            var region26 = new Region(26, afrika);
            var region27 = new Region(27, azia);
            var region28 = new Region(28, azia);
            var region29 = new Region(29, azia);
            var region30 = new Region(30, azia);
            var region31 = new Region(31, azia);
            var region32 = new Region(32, azia);
            var region33 = new Region(33, azia);
            var region34 = new Region(34, azia);
            var region35 = new Region(35, azia);
            var region36 = new Region(36, azia);
            var region37 = new Region(37, azia);
            var region38 = new Region(38, azia);
            var region39 = new Region(39, australia);
            var region40 = new Region(40, australia);
            var region41 = new Region(41, australia);
            var region42 = new Region(42, australia);

            region1.AddNeighbor(region2);
            region1.AddNeighbor(region4);
            region1.AddNeighbor(region30);
            region2.AddNeighbor(region4);
            region2.AddNeighbor(region3);
            region2.AddNeighbor(region5);
            region3.AddNeighbor(region5);
            region3.AddNeighbor(region6);
            region3.AddNeighbor(region14);
            region4.AddNeighbor(region5);
            region4.AddNeighbor(region7);
            region5.AddNeighbor(region6);
            region5.AddNeighbor(region7);
            region5.AddNeighbor(region8);
            region6.AddNeighbor(region8);
            region7.AddNeighbor(region8);
            region7.AddNeighbor(region9);
            region8.AddNeighbor(region9);
            region9.AddNeighbor(region10);
            region10.AddNeighbor(region11);
            region10.AddNeighbor(region12);
            region11.AddNeighbor(region12);
            region11.AddNeighbor(region13);
            region12.AddNeighbor(region13);
            region12.AddNeighbor(region21);
            region14.AddNeighbor(region15);
            region14.AddNeighbor(region16);
            region15.AddNeighbor(region16);
            region15.AddNeighbor(region18);
            region15.AddNeighbor(region19);
            region16.AddNeighbor(region17);
            region17.AddNeighbor(region19);
            region17.AddNeighbor(region20);
            region17.AddNeighbor(region27);
            region17.AddNeighbor(region32);
            region17.AddNeighbor(region36);
            region18.AddNeighbor(region19);
            region18.AddNeighbor(region20);
            region18.AddNeighbor(region21);
            region19.AddNeighbor(region20);
            region20.AddNeighbor(region21);
            region20.AddNeighbor(region22);
            region20.AddNeighbor(region36);
            region21.AddNeighbor(region22);
            region21.AddNeighbor(region23);
            region21.AddNeighbor(region24);
            region22.AddNeighbor(region23);
            region22.AddNeighbor(region36);
            region23.AddNeighbor(region24);
            region23.AddNeighbor(region25);
            region23.AddNeighbor(region26);
            region23.AddNeighbor(region36);
            region24.AddNeighbor(region25);
            region25.AddNeighbor(region26);
            region27.AddNeighbor(region28);
            region27.AddNeighbor(region32);
            region27.AddNeighbor(region33);
            region28.AddNeighbor(region29);
            region28.AddNeighbor(region31);
            region28.AddNeighbor(region33);
            region28.AddNeighbor(region34);
            region29.AddNeighbor(region30);
            region29.AddNeighbor(region31);
            region30.AddNeighbor(region31);
            region30.AddNeighbor(region34);
            region30.AddNeighbor(region35);
            region31.AddNeighbor(region34);
            region32.AddNeighbor(region33);
            region32.AddNeighbor(region36);
            region32.AddNeighbor(region37);
            region33.AddNeighbor(region34);
            region33.AddNeighbor(region37);
            region33.AddNeighbor(region38);
            region34.AddNeighbor(region35);
            region36.AddNeighbor(region37);
            region37.AddNeighbor(region38);
            region38.AddNeighbor(region39);
            region39.AddNeighbor(region40);
            region39.AddNeighbor(region41);
            region40.AddNeighbor(region41);
            region40.AddNeighbor(region42);
            region41.AddNeighbor(region42);

           map.Add(region1);
           map.Add(region2);
           map.Add(region3);
           map.Add(region4);
           map.Add(region5);
           map.Add(region6);
           map.Add(region7);
           map.Add(region8);
           map.Add(region9);
           map.Add(region10);
           map.Add(region11);
           map.Add(region12);
           map.Add(region13);
           map.Add(region14);
           map.Add(region15);
           map.Add(region16);
           map.Add(region17);
           map.Add(region18);
           map.Add(region19);
           map.Add(region20);
           map.Add(region21);
           map.Add(region22);
           map.Add(region23);
           map.Add(region24);
           map.Add(region25);
           map.Add(region26);
           map.Add(region27);
           map.Add(region28);
           map.Add(region29);
           map.Add(region30);
           map.Add(region31);
           map.Add(region32);
           map.Add(region33);
           map.Add(region34);
           map.Add(region35);
           map.Add(region36);
           map.Add(region37);
           map.Add(region38);
           map.Add(region39);
           map.Add(region40);
           map.Add(region41);
           map.Add(region42);
           map.Add(northAmerica);
           map.Add(southAmerica);
           map.Add(europe);
           map.Add(afrika);
           map.Add(azia);
           map.Add(australia);

            return map;
        }

        //Make every region neutral with 2 armies to start with
        private Map setupMap(Map initMap)
        {
            Map map = initMap;
            foreach (var region in map.Regions)
            {
                region.PlayerName = ("neutral");
                region.Armies = (2);
            }
            return map;
        }

        private void SendSetupMapInfo(IRobot bot, Map initMap)
        {
            string setupSuperRegionsString = getSuperRegionsString(initMap);
            string setupRegionsString = getRegionsString(initMap);
            string setupNeighborsString = GetNeighborsString(initMap);

            bot.WriteInfo(setupSuperRegionsString);
            // Console.WriteLine(setupSuperRegionsString);
            bot.WriteInfo(setupRegionsString);
            // Console.WriteLine(setupRegionsString);
            bot.WriteInfo(setupNeighborsString);
            // Console.WriteLine(setupNeighborsString);
        }

        private String getSuperRegionsString(Map map)
        {
            String superRegionsString = "setup_map super_regions";
            foreach (var superRegion in map.SuperRegions)
            {
                int id = superRegion.Id;
                int reward = superRegion.ArmiesReward;
                superRegionsString += " " + id + " " + reward;
            }
            return superRegionsString;
        }

        private String getRegionsString(Map map)
        {
            String regionsString = "setup_map regions";
            foreach (var region in map.Regions)
            {
                int id = region.Id;
                int superRegionId = region.SuperRegion.Id;
                regionsString += " " + id + " " + superRegionId;
            }
            return regionsString;
        }

        //beetje inefficiente methode, maar kan niet sneller wss
        private String GetNeighborsString(Map map)
        {
            String neighborsString = "setup_map neighbors";
            var doneList = new List<Point>();
            foreach (var region in map.Regions)
            {
                int id = region.Id;
                String neighbors = "";
                foreach (var neighbor in region.Neighbors)
                {
                    if (CheckDoneList(doneList, id, neighbor.Id))
                    {
                        neighbors += "," + neighbor.Id;
                        doneList.Add(new Point(id, neighbor.Id));
                    }
                }
                if (neighbors.Length != 0)
                {
                    neighbors = neighbors.ReplaceFirst(",", " ");
                    neighborsString += " " + id + neighbors;
                }
            }
            return neighborsString;
        }

        private Boolean CheckDoneList(IEnumerable<Point> doneList, int regionId, int neighborId)
        {
            return doneList.All(p => (p.X != regionId || p.Y != neighborId) && (p.X != neighborId || p.Y != regionId));
        }

        //private String GetPlayedGame(Player winner, String gameView)
        //{
        //    var output = new StringBuilder();

        //    List<MoveResult> playedGame;
        //    if (gameView.Equals("player1"))
        //        playedGame = _player1PlayedGame;
        //    else if (gameView.Equals("player2"))
        //        playedGame = _player2PlayedGame;
        //    else
        //        playedGame = _fullPlayedGame;

        //    playedGame.Remove(playedGame.Last());
        //    int roundNr = 2;
        //    output.Append("map " + playedGame.First().Map.GetMapString() + "\n");
        //    output.Append("round 1" + "\n");
        //    foreach (var moveResult in playedGame)
        //    {
        //        if (moveResult != null)
        //        {
        //            if (moveResult.Move != null)
        //            {
        //                try
        //                {
        //                    var plm = (PlaceArmiesMove) moveResult.Move;
        //                    output.Append(plm.GetString() + "\n");
        //                }
        //                catch (Exception)
        //                {
        //                    var atm = (AttackTransferMove) moveResult.Move;
        //                    output.Append(atm.GetString() + "\n");
        //                }
        //                output.Append("map " + moveResult.Map.GetMapString() + "\n");
        //            }
        //        }
        //        else
        //        {
        //            output.Append("round " + roundNr + "\n");
        //            roundNr++;
        //        }
        //    }

        //    if (winner != null)
        //        output.Append(winner.Name + " won\n");
        //    else
        //        output.Append("Nobody won\n");

        //    return output.ToString();
        //}

        //private String CompressGZip(String data, String outFile)
        //{
        //    try
        //    {
        //        var fos = new FileStream(outFile);
        //        GZIPOutputStream gzos = new GZIPOutputStream(fos);

        //        byte[] outBytes = data.getBytes("UTF-8");
        //        gzos.write(outBytes, 0, outBytes.length);

        //        gzos.finish();
        //        gzos.close();

        //        return outFile;
        //    }
        //    catch (IOException e)
        //    {
        //        Console.WriteLine(e.Message);
        //        return "";
        //    }
        //}

        /*
	 * MongoDB connection functions
	 */

        public void SaveGame(IORobot bot1, IORobot bot2, int gameNumber)
        {   
            Player winner = _engine.WinningPlayer();
            if (winner.Name.Equals("player1")) _player1Wins++;
            if (winner.Name.Equals("player2")) _player2Wins++;
            int score = _engine.RoundNumber - 1;
            WriteFile("{0}Game_{1}_{2}_Wins_Bot{3}_Dump.txt", bot1.GetDump() + string.Format("{0} wins on round {1}", winner.Name, score), 1, gameNumber);
            WriteFile("{0}Game_{1}_{2}_Wins_Bot{3}_Dump.txt", bot2.GetDump() + string.Format("{0} wins on round {1}", winner.Name, score), 2, gameNumber);
            WriteFile("{0}Game_{1}_{2}_Wins_Bot{3}_err.txt", bot1.GetGameLogs()[1], 1, gameNumber);
            WriteFile("{0}Game_{1}_{2}_Wins_Bot{3}_err.txt", bot2.GetGameLogs()[1], 2, gameNumber);
        }

        public void WriteFile(string fileFormat, string dump, int bot, int gameNumber)
        {

            File.WriteAllText(string.Format(fileFormat, ConfigurationManager.AppSettings["outputDirectory"], gameNumber, _engine.WinningPlayer().Name, bot), dump);
            
        }
    }
}