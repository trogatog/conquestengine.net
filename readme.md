Set your output directory in the app.config file

run ConquestEngine <firstbot.exe> <secondbot.exe> <# of rounds to run>

if you don't supply a # of rounds to run, it defaults to 1

Have it set to rerun matches when stalemates happen *(normal if you're running my-own-bot-on-my-own-bot action)*

**If you want to run it and view the stalemates, comment out the if statement on line 131 of RunGame.cs**

Enjoy!