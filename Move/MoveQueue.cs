﻿// Copyright 2014 theaigames.com (developers@theaigames.com)
// Conversion to .Net by Mark van Oudheusden (m.w.vano@gmail.com)
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at

//        http://www.apache.org/licenses/LICENSE-2.0

//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
//	
//    For the full copyright and license information, please view the LICENSE
//    file that was distributed with this source code.
using System;
using System.Collections.Generic;
using System.Linq;
using Model;

namespace Move
{
    public class MoveQueue
    {

        public List<PlaceArmiesMove> PlaceArmiesMoves;
        public List<AttackTransferMove> AttackTransferMovesP1;
        public List<AttackTransferMove> AttackTransferMovesP2;
        private readonly Player _player1;
        private readonly Player _player2;
        // public final int ORDER_RANDOM = 1;
        // public final int ORDER_CYCLIC = 2;

        public MoveQueue(Player player1, Player player2)
        {
            PlaceArmiesMoves = new List<PlaceArmiesMove>();
            AttackTransferMovesP1 = new List<AttackTransferMove>();
            AttackTransferMovesP2 = new List<AttackTransferMove>();
            _player1 = player1;
            _player2 = player2;
        }

        public void AddMove(Model.Move move)
        {
            try
            { //add PlaceArmiesMove
                var plm = (PlaceArmiesMove)move;
                PlaceArmiesMoves.Add(plm);
            }
            catch (Exception)
            { //add AttackTransferMove
                var atm = (AttackTransferMove)move;
                if (_player1.Name.Equals(move.GetPlayerName()))
                    AttackTransferMovesP1.Add(atm);
                else if (_player2.Name.Equals(move.GetPlayerName()))
                    AttackTransferMovesP2.Add(atm);
            }
        }

        public void Clear()
        {
            PlaceArmiesMoves = new List<PlaceArmiesMove>();
            AttackTransferMovesP1 = new List<AttackTransferMove>();
            AttackTransferMovesP2 = new List<AttackTransferMove>();
        }

        public bool HasNextAttackTransferMove()
        {
            if (AttackTransferMovesP1.Count == 0 && AttackTransferMovesP2.Count == 0)
                return false;
            return true;
        }

        //the player's moves are still in the same order, but here is determined which player moves first.
        //player to move first each move is chosen random.
        //makes sure that if a player has an illegal move, it is the next legal move is selected.
        public AttackTransferMove GetNextAttackTransferMove(int moveNr, String previousMovePlayer, Boolean previousWasIllegal)
        {
            if (!HasNextAttackTransferMove()) //shouldnt ever happen
            {
                Console.Error.WriteLine("No more AttackTransferMoves left in MoveQueue");
                return null;
            }

            if (!previousWasIllegal)
            {
                if (moveNr % 2 == 1 || previousMovePlayer.Equals("")) //first move of the two
                {
                    var random = new Random();
                    double rand = random.NextDouble();
                    return GetMove(rand < 0.5);
                }
                return GetMove(previousMovePlayer.Equals(_player2.Name));
            }
            return GetMove(previousMovePlayer.Equals(_player1.Name));
        }

        private AttackTransferMove GetMove(Boolean conditionForPlayer1)
        {
            AttackTransferMove move;
            if (AttackTransferMovesP1.Any() && (conditionForPlayer1 || !AttackTransferMovesP2.Any())) //get player1's move
            {
                move = AttackTransferMovesP1.First();
                AttackTransferMovesP1.Remove(move);
                return move;
            }
            
            move = AttackTransferMovesP2.First();
            AttackTransferMovesP2.Remove(move);
            return move;
        }

        //the player's moves are still in the same order, but here is determined which player moves first.
        //if orderingType is ORDER_RANDOM, player to move first each move is chosen random.
        //if orderingType is ORDER_CYCLIC, every round an other player moves first on every move.

        //not used anymore
        // public void orderMoves(int roundNr, int orderingType)
        // {
        // 	if(!attackTransferMoves.isEmpty())
        // 	{
        // 		List<AttackTransferMove> orderedMoves = new List<AttackTransferMove>();
        // 		int p = nrOfMovesP1;
        // 		int i = 0;

        // 		while(true)
        // 		{
        // 			if(i >= p) //when player2 has more moves than player1
        // 			{
        // 				for(int j=i+p; j<attackTransferMoves.size(); j++)
        // 					orderedMoves.add(attackTransferMoves.get(j)); //add remaining moves to queue
        // 				break;
        // 			}
        // 			if(i+p >= p+nrOfMovesP2) //when player1 has more moves than player2
        // 			{
        // 				for(int j=i; j<p; j++)
        // 					orderedMoves.add(attackTransferMoves.get(j)); //add remaining moves to queue
        // 				break;
        // 			}

        // 			double rand = Math.random();
        // 			if((orderingType == ORDER_RANDOM && rand < 0.5) || (orderingType == ORDER_CYCLIC && roundNr%2 == 1))
        // 			{
        // 				orderedMoves.add(attackTransferMoves.get(i)); 	//player1's move
        // 				orderedMoves.add(attackTransferMoves.get(i+p));	//player2's move
        // 			}
        // 			else
        // 			{
        // 				orderedMoves.add(attackTransferMoves.get(i+p)); //player2's move
        // 				orderedMoves.add(attackTransferMoves.get(i));	//player1's move
        // 			}

        // 			i++;
        // 		}

        // 		attackTransferMoves = orderedMoves;
        // 	}
        // }


    }

}
