﻿// Copyright 2014 theaigames.com (developers@theaigames.com)
// Conversion to .Net by Mark van Oudheusden (m.w.vano@gmail.com)
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at

//        http://www.apache.org/licenses/LICENSE-2.0

//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
//	
//    For the full copyright and license information, please view the LICENSE
//    file that was distributed with this source code.
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Threading;

namespace IO
{
    public class IOHandler
    {
        private readonly Process _child;
        private string _output;
        private int _current;
        private bool _newDataReceived;
        private readonly StringBuilder _err;
        private readonly StringBuilder _gameLog;
        private readonly StreamWriter _input;

        public IOHandler(String command)
        {
            //Info on properly redirecting:
            //http://stackoverflow.com/questions/415620/redirect-console-output-to-textbox-in-separate-program
            Console.WriteLine(command);
            _err = new StringBuilder();
            _gameLog = new StringBuilder();
            _current = 0;
            _newDataReceived = true;
            _child = new Process
            {
                StartInfo =
                {
                    UseShellExecute = false,
                    FileName = command,
                    RedirectStandardError = true,
                    RedirectStandardInput = true,
                    RedirectStandardOutput = true
                }
            };
            _child.ErrorDataReceived += Child_ErrorDataReceived;
            _child.OutputDataReceived += Child_DataRecieved;
            _child.Start();
            while (!_child.Responding)
            {
                Thread.Sleep(200);
            }
            _input = _child.StandardInput;
            _child.BeginOutputReadLine();
            _child.BeginErrorReadLine();
        }

        public void Stop()
        {
            try
            {
                _input.Close();
            }
            catch (IOException)
            {
            }
            _child.Close();
        }

        void Child_ErrorDataReceived(object sender, DataReceivedEventArgs e)
        {
            _err.Append(e.Data + "\r\n");
        }

        public void Child_DataRecieved(object sender, DataReceivedEventArgs e)
        {
            _newDataReceived = true;
            _output =  e.Data;
            _gameLog.Append(_output + "\r\n");
        }
        public string ReadLine(long timeOut)
        {
            int pauseCount = 0;
            while (!_newDataReceived && pauseCount <= 200)
            {
                Thread.Sleep(20);
                pauseCount++;
            }
            _newDataReceived = false;
            return _output;
        }

        public bool WriteLine(String line)
        {
            if (!IsRunning())
            {
                return false;
            }
            try
            {
                _input.WriteLine(line.Trim());
                return true;
            }
            catch (IOException)
            {
                return false;
            }

        }

        public bool IsRunning()
        {
            return _child.Responding;
        }

        public List<string> GetGameLogs()
        {
            return new List<string> { _gameLog.ToString(), _err.ToString() };
        }
    }
}
