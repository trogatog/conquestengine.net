﻿// Copyright 2014 theaigames.com (developers@theaigames.com)
// Conversion to .Net by Mark van Oudheusden (m.w.vano@gmail.com)
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at

//        http://www.apache.org/licenses/LICENSE-2.0

//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
//	
//    For the full copyright and license information, please view the LICENSE
//    file that was distributed with this source code.
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Model;

namespace IO
{
    public class IORobot : IRobot
    {
        private readonly IOHandler _handler;
        private readonly StringBuilder _dump;
        private int _errorCounter;
        private const int MAX_ERRORS = 2;

        public IORobot(String command)
        {
            try
            {
                _handler = new IOHandler(command);
                _dump = new StringBuilder();
                _errorCounter = 0;
            }
            catch (IOException)
            {
            }
        }


        public void Setup(long timeOut)
        {
        }


        public String GetPreferredStartingArmies(long timeOut, IEnumerable<Region> pickableRegions)
        {
            String output = "pick_starting_regions " + timeOut;
            foreach (var region in pickableRegions)
                output += " " + region.Id;

            _handler.WriteLine(output);
            string line = null;
            while(line == null)
                line = _handler.ReadLine(timeOut);
            _dump.Append(output + "\r\n");
            _dump.Append(line + "\r\n");
            return line;
        }


        public String GetPlaceArmiesMoves(long timeOut)
        {
            return GetMoves("place_armies", timeOut);
        }


        public String GetAttackTransferMoves(long timeOut)
        {
            return GetMoves("attack/transfer", timeOut);
        }

        private String GetMoves(String moveType, long timeOut)
        {
            String line = "";
            if (_errorCounter < MAX_ERRORS)
            {
                _handler.WriteLine("go " + moveType + " " + timeOut);
                _dump.Append("go " + moveType + " " + timeOut + "\r\n");

                while (line != null && line.Length < 1)
                {
                    const long timeElapsed = 0;// timeNow - timeStart;
                    line = _handler.ReadLine(timeOut);
                    _dump.Append(line + "\r\n");
                    if (timeElapsed >= timeOut)
                        break;
                }
                if (line == null)
                {
                    _errorCounter++;
                    return "";
                }
                if (line.Equals("No moves"))
                    return "";
            }
            else
            {
                _dump.Append("go " + moveType + " " + timeOut + "\n");
                _dump.Append(
                    "Maximum number of idle moves returned: skipping move (let bot return 'No moves' instead of nothing)");
            }
            return line;
        }


        public void WriteInfo(String info)
        {
            _handler.WriteLine(info);
            _dump.Append(info + "\n");
        }

        public void AddToDump(String dumpy)
        {
            _dump.Append(dumpy + "\r\n");
        }


        public void Finish()
        {
            _handler.Stop();
        }

        public String GetDump()
        {
            return _dump.ToString();
        }

        public List<string> GetGameLogs()
        {
            return _handler.GetGameLogs();
        }
    }
}