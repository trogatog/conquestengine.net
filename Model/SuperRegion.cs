﻿// Copyright 2014 theaigames.com (developers@theaigames.com)
// Conversion to .Net by Mark van Oudheusden (m.w.vano@gmail.com)
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at

//        http://www.apache.org/licenses/LICENSE-2.0

//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
//	
//    For the full copyright and license information, please view the LICENSE
//    file that was distributed with this source code.
using System;
using System.Collections.Generic;
using System.Linq;

namespace Model
{
    public class SuperRegion
    {
        public SuperRegion(int id, int armiesReward)
        {
            Id = id;
            ArmiesReward = armiesReward;
            SubRegions = new List<Region>();
        }

        public void AddSubRegion(Region subRegion)
        {
            if (!SubRegions.Contains(subRegion))
                SubRegions.Add(subRegion);
        }

        /**
         * @return A string with the name of the player that fully owns this SuperRegion
         */

        public String OwnedByPlayer()
        {
            var playerName = SubRegions.First().PlayerName;
            return SubRegions.Any(region => !playerName.Equals(region.PlayerName)) ? null : playerName;
        }

        public int Id { get; private set; }

        public int ArmiesReward { get; private set; }

        public List<Region> SubRegions { get; private set; }
    }
}
