﻿// Copyright 2014 theaigames.com (developers@theaigames.com)
// Conversion to .Net by Mark van Oudheusden (m.w.vano@gmail.com)
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at

//        http://www.apache.org/licenses/LICENSE-2.0

//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
//	
//    For the full copyright and license information, please view the LICENSE
//    file that was distributed with this source code.
using System;
using System.Collections.Generic;
using System.Linq;

namespace Model
{
    public class Map
    {

        public List<Region> Regions { get; private set; }
        public List<SuperRegion> SuperRegions { get; private set; }

        public Map()
        {
            Regions = new List<Region>();
            SuperRegions = new List<SuperRegion>();
        }

        public Map(List<Region> regions, List<SuperRegion> superRegions)
        {
            Regions = regions;
            SuperRegions = superRegions;
        }

        /**
         * add a Region to the map
         * @param region in Region to be added
         */

        public void Add(Region region)
        {
            if (Regions.Any(r => r.Id == region.Id))
            {
                Console.Error.WriteLine("Region cannot be addedin id already exists.");
                return;
            }
            Regions.Add(region);
        }

        /**
         * add a SuperRegion to the map
         * @param superRegion in SuperRegion to be added
         */

        public void Add(SuperRegion superRegion)
        {
            if (SuperRegions.Any(s => s.Id == superRegion.Id))
            {
                Console.Error.WriteLine("SuperRegion cannot be addedin id already exists.");
                return;
            }
            SuperRegions.Add(superRegion);
        }

        /**
         * @return in a new Map object exactly the same as this one
         */

        public Map GetMapCopy()
        {
            var newMap = new Map();
            foreach (var sr in SuperRegions) //copy superRegions
            {
                var newSuperRegion = new SuperRegion(sr.Id, sr.ArmiesReward);
                newMap.Add(newSuperRegion);
            }
            foreach (var r in Regions) //copy regions
            {
                var newRegion = new Region(r.Id, newMap.GetSuperRegionById(r.SuperRegion.Id),
                    r.PlayerName, r.Armies);
                newMap.Add(newRegion);
            }
            foreach (var r in Regions) //add neighbors to copied regions
            {
                var newRegion = newMap.GetRegionById(r.Id);
                foreach (var neighbor in r.Neighbors)
                    newRegion.AddNeighbor(newMap.GetRegionById(neighbor.Id));
            }
            return newMap;
        }

        /**
         * @return in the list of all Regions in this map
         */

        public List<Region> GetRegions()
        {
            return Regions;
        }

        /**
         * @param id in a Region id number
         * @return in the matching Region object
         */

        public Region GetRegionById(int id)
        {
            foreach (var region in Regions)
                if (region.Id == id)
                    return region;
            Console.Error.WriteLine("Could not find region with id " + id);
            return null;
        }

        /**
         * @param id in a SuperRegion id number
         * @return in the matching SuperRegion object
         */

        public SuperRegion GetSuperRegionById(int id)
        {
            foreach (var superRegion in SuperRegions)
                if (superRegion.Id == id)
                    return superRegion;
            Console.Error.WriteLine("Could not find superRegion with id " + id);
            return null;
        }

        public String GetMapString()
        {
            String mapString = "";
            foreach (var region in Regions)
            {
                mapString += region.Id + ";" + region.PlayerName + ";" + region.Armies + " ";
            }
            return mapString;
        }

        //return all regions owned by given player
        public List<Region> OwnedRegionsByPlayer(Player player)
        {
            var ownedRegions = new List<Region>();

            foreach (Region region in GetRegions())
                if (region.PlayerName.Equals(player.Name))
                    ownedRegions.Add(region);

            return ownedRegions;
        }

        //fog of war
        //return all regions visible to given player
        public List<Region> VisibleRegionsForPlayer(Player player)
        {
            var visibleRegions = new List<Region>();
            List<Region> ownedRegions = OwnedRegionsByPlayer(player);

            visibleRegions.AddRange(ownedRegions);

            foreach (var region in ownedRegions)
                foreach (var neighbor in region.Neighbors)
                    if (!visibleRegions.Contains(neighbor))
                        visibleRegions.Add(neighbor);

            return visibleRegions;
        }

        public Map GetVisibleMapCopyForPlayer(Player player)
        {
            Map visibleMap = GetMapCopy();
            List<Region> visibleRegions = VisibleRegionsForPlayer(player);

            foreach (Region region in Regions)
            {
                if (!visibleRegions.Contains(region))
                {
                    Region unknownRegion = visibleMap.GetRegionById(region.Id);
                    unknownRegion.PlayerName = ("unknown");
                    unknownRegion.Armies = (0);
                }
            }

            return visibleMap;
        }

    }
}