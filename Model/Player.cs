﻿// Copyright 2014 theaigames.com (developers@theaigames.com)
// Conversion to .Net by Mark van Oudheusden (m.w.vano@gmail.com)
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at

//        http://www.apache.org/licenses/LICENSE-2.0

//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
//	
//    For the full copyright and license information, please view the LICENSE
//    file that was distributed with this source code.
using System;

namespace Model
{
    public class Player
    {
        public Player(String name, IRobot bot, int startingArmies)
        {
            Name = name;
            Bot = bot;
            ArmiesPerTurn = startingArmies; //start with 5 armies per turn
        }

        public string Name { get; private set; }

        public IRobot Bot { get; private set; }

        public int ArmiesPerTurn { get; private set; }

        public int ArmiesLeft { get; set; }
    }

}
