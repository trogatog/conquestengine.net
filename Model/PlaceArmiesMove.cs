﻿// Copyright 2014 theaigames.com (developers@theaigames.com)
// Conversion to .Net by Mark van Oudheusden (m.w.vano@gmail.com)
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at

//        http://www.apache.org/licenses/LICENSE-2.0

//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
//	
//    For the full copyright and license information, please view the LICENSE
//    file that was distributed with this source code.
using System;

namespace Model
{
    public class PlaceArmiesMove : Move
    {

        private readonly Region _region;
        private int _armies;

        //geen misbruik maken van playerName aub, alleen je eigen botnaam invullen
        public PlaceArmiesMove(String playerName, Region region, int armies)
        {
            SetPlayerName(playerName);
            _region = region;
            _armies = armies;
        }


        public Region Region
        {
            get { return _region; }
        }

        public int Armies
        {
            get { return _armies; }
            set { _armies = value; }
        }

        /**
	 * @return A string representation of this Move
	 */

        public string GetString()
        {
            if (GetIllegalMove().Equals(string.Empty))
                return GetPlayerName() + " place_armies " + _region.Id + " " + _armies;
            return GetPlayerName() + " illegal_move " + GetIllegalMove();
        }
    }

}