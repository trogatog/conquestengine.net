﻿// Copyright 2014 theaigames.com (developers@theaigames.com)
// Conversion to .Net by Mark van Oudheusden (m.w.vano@gmail.com)
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at

//        http://www.apache.org/licenses/LICENSE-2.0

//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
//	
//    For the full copyright and license information, please view the LICENSE
//    file that was distributed with this source code.
using System;
using System.Collections.Generic;

namespace Model
{
    public class Region
    {
        private readonly List<Region> _neighbors;

        public Region(int id, SuperRegion superRegion)
        {
            Id = id;
            SuperRegion = superRegion;
            _neighbors = new List<Region>();
            PlayerName = "unknown";
            Armies = 0;

            superRegion.AddSubRegion(this);
        }

        public Region(int id, SuperRegion superRegion, String playerName, int armies)
        {
            Id = id;
            SuperRegion = superRegion;
            _neighbors = new List<Region>();
            PlayerName = playerName;
            Armies = armies;

            superRegion.AddSubRegion(this);
        }

        public void AddNeighbor(Region neighbor)
        {
            if (!_neighbors.Contains(neighbor))
            {
                _neighbors.Add(neighbor);
                neighbor.AddNeighbor(this);
            }
        }

        /**
         * @param region a Region object
         * @return True if this Region is a neighbor of given Region, false otherwise
         */

        public bool IsNeighbor(Region region)
        {
            return _neighbors.Contains(region);
        }

        /**
         * @param playerName A string with a player's name
         * @return True if this region is owned by given playerName, false otherwise
         */

        public bool OwnedByPlayer(String playerName)
        {
            return playerName.Equals(PlayerName);
        }

        public int Id { get; private set; }

        public IEnumerable<Region> Neighbors{get { return _neighbors; }} 

        // Copyright 2014 theaigames.com (developers@theaigames.com)

        //    Licensed under the Apache License, Version 2.0 (the "License");
        //    you may not use this file except in compliance with the License.
        //    You may obtain a copy of the License at

        //        http://www.apache.org/licenses/LICENSE-2.0

        //    Unless required by applicable law or agreed to in writing, software
        //    distributed under the License is distributed on an "AS IS" BASIS,
        //    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
        //    See the License for the specific language governing permissions and
        //    limitations under the License.
        //	
        //    For the full copyright and license information, please view the LICENSE
        //    file that was distributed with this source code.

        public SuperRegion SuperRegion { get; private set; }

        public int Armies { get; set; }

        public string PlayerName { get; set; }
    }
}
